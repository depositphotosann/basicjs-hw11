"use strict"

/*
Теоретичні питання
 1. Що таке події в JavaScript і для чого вони використовуються?

В JavaScript події (events) представляють собою спеціальні моменти або дії, які відбуваються під час взаємодії користувача з веб-сторінкою чи під час виникнення інших подій в середовищі виконання JavaScript.
Приклади подій включають натискання кнопки миші, клавіші на клавіатурі, завантаження сторінки, зміна розміру вікна браузера та інші події, що відбуваються в середовищі виконання.
Події використовуються для обробки реакцій на взаємодію користувача та забезпечення динамічності веб-сторінок. Для обробки подій в JavaScript використовуються обробники подій (event handlers). 
Обробник події - це функція, яка викликається автоматично, коли відбувається певна подія. Наприклад, обробник події "click" викликається, коли користувач натискає кнопку миші.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
В JavaScript існує багато подій миші, які можна обробляти. Ось деякі з найпоширеніших подій миші:

1) click: Виникає при натисканні лівої кнопки миші.
element.addEventListener('click', function() {
    console.log('Клікнуто!');
});

2) dblclick: Виникає при подвійному натисканні лівої кнопки миші.
element.addEventListener('dblclick', function() {
    console.log('Подвійний клік!');
});

3) mousedown: Виникає при натисканні будь-якої кнопки миші.
element.addEventListener('mousedown', function() {
    console.log('Кнопка миші натиснута!');
});

4) mouseup: Виникає при відпусканні будь-якої кнопки миші.
element.addEventListener('mouseup', function() {
    console.log('Кнопка миші відпущена!');
});

5) mousemove: Виникає при русі миші над елементом.
element.addEventListener('mousemove', function(event) {
    console.log('Рух миші. Координати: ' + event.clientX + ', ' + event.clientY);
});

6) mouseover і mouseout: Виникають при наведенні миші на елемент та виходженні миші за межі елемента відповідно.
element.addEventListener('mouseover', function() {
    console.log('Миша в надійшла на елемент!');
});

element.addEventListener('mouseout', function() {
    console.log('Миша виходить за межі елемента!');
});

Ці події дозволяють вам реагувати на взаємодію користувача з мишею і виконувати відповідні дії в залежності від контексту вашого додатку чи веб-сторінки.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
Подія "contextmenu" в JavaScript виникає при виклику контекстного меню (наприклад, правою кнопкою миші) на елементі. Ця подія дозволяє вам визначити власну логіку обробки для відкриття контекстного меню та виконання певних дій під час його відображення.
Простий приклад використання події "contextmenu" для заборони відображення браузерного контекстного меню виводить сповіщення про власне контекстне меню:
element.addEventListener('contextmenu', function(event) {
    event.preventDefault(); // Забороняє відображення браузерного контекстного меню
    alert('Моє власне контекстне меню!');
});
У цьому прикладі event.preventDefault() використовується для того, щоб заборонити відображення стандартного браузерного контекстного меню. Після цього виводиться сповіщення про власне контекстне меню.
Ця подія може бути корисною, коли потрібно виконати певні дії, такі як відображення власного контекстного меню, при натисканні правої кнопки миші на певному елементі в вашому додатку чи на веб-сторінці.
*/


/*
Практичні завдання
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
*/

// document.getElementById('btn-click').onclick = () => {
//     const newParagraph = document.createElement('p');
//     newParagraph.textContent = 'New Paragraph';
//     const contentSection = document.getElementById('content');
//     contentSection.appendChild(newParagraph);
// };

document.getElementById('btn-click').addEventListener('click', () => {
    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'New Paragraph';
    const contentSection = document.getElementById('content');
    contentSection.appendChild(newParagraph);
});

/*
2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, 
наприклад, type, placeholder, і name. та додайте його під кнопкою.
*/

document.getElementById('btn-input-create').addEventListener('click', () => {
    const newInput = document.createElement('input');
    newInput.type = 'text';
    newInput.placeholder = 'Enter text';
    newInput.name = 'newInput';

    const container = document.querySelector('#element-container');
    container.append(newInput);
});